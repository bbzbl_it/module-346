module "M346_docker_host" {
  source                    = "./modules/linode/alma9_docker"

  linode_token              = var.linode_token

  vm_label                  = "M346_docker_host"
  vm_tags                   = ["M346"]
  vm_group                  = "M346"
  linode_region             = "eu-central"
  linude_vm_type            = 1
  vm_swap_size              = 1024
  
  vm_root_password          = var.vm_root_password
  vm_root_authorized_keys   = var.vm_root_authorized_keys

  ansible_playbook_file     = "alma9_docker.yml"
  ansible_vault_password    = var.ansible_vault_password

  duck_dns_domain           = var.duck_dns_domain
  duck_dns_token            = var.duck_dns_token
}