# Provider Variabels
variable "linode_token" {
  description = "The API token to authenticate to the linode API."
  type = string
  sensitive = true
}

variable "vm_root_password" {
  description = "The Password for the root user on the VM."
  sensitive = true
  type = string
}

variable "vm_root_authorized_keys" {
  description = "The ssh key(s) for the root user on the VM."
  sensitive = true
  type = list(string)
  default = []
}

variable "ansible_vault_password" {
  description = "The Password that ansible uses to decript the vault file."
  type = string
  sensitive = true 
}

variable "duck_dns_domain" {
    description = "The domain on duckdns."
    type = string
}

variable "duck_dns_token" {
    description = "The token to access duckdns."
    type = string
    sensitive = true 
}