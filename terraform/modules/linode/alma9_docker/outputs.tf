output "linode_instance_alma9_docker_ip_address" {
    value = linode_instance.alma9_docker.ip_address
    description = "The IP address of the VM."
}
