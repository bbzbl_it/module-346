resource "linode_instance" "alma9_docker" {
    label = var.vm_label
    group = var.vm_group
    tags = local.vm_all_tags

    image = var.vm_image
    type = local.vm_type

    region = var.linode_region

    swap_size = var.vm_swap_size

    root_pass = var.vm_root_password
    authorized_keys = var.vm_root_authorized_keys

    #watchdog_enabled = false

    connection {
        type     = "ssh"
        user     = "root"
        password = var.vm_root_password
        host     = self.ip_address
    }

    provisioner "local-exec" {
        command = "curl 'https://www.duckdns.org/update?domains=${var.duck_dns_domain}&token=${var.duck_dns_token}&ip=${self.ip_address}'"
    }

    provisioner "local-exec" {
        working_dir = "/tmp"
        command = "echo '${var.ansible_vault_password}' > ansible_vault_pw && chmod 600 ansible_vault_pw ; echo '${var.vm_root_password}' > ansible_ssh_pw && chmod 600 ansible_ssh_pw ; sleep 10"
    }

    provisioner "local-exec" {
        working_dir = "../ansible"
        command = <<-EOT
            ansible-playbook -i '${self.ip_address},' -u 'root' --vault-password-file '/tmp/ansible_vault_pw' --connection-password-file '/tmp/ansible_ssh_pw' ${var.ansible_playbook_file} 
        EOT
    }

    provisioner "local-exec" {
        working_dir = "/tmp"
        command = "rm ansible_vault_pw && rm ansible_ssh_pw"
    }

}
