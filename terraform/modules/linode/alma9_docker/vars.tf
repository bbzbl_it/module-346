# Provider Variables
variable "linode_token" {
    description = "The API token that is used to connect and authenticate to linode."
    type = string
}


##############################################

# Resource Variables

variable "vm_label" {
  description = "The Name of the VM that is created."
  type = string
}

variable "vm_tags" {
  description = "List of tags for the new VM."
  type        = list(string)
  default     = ["terraform", "almalinux9"]
}

variable "vm_group" {
  description = "The envoirement of the VM that is created"
  type        = string
  default     = "tst"
}

variable "linode_region" {
  description = "The Name of the target Proxmox node."
  type        = string
  default     = "eu-central"
}

variable "vm_image" {
  description = "The image for the VM."
  type        = string
    default = "linode/almalinux9"
}

variable "linude_vm_type" {
  description = "Specifies the Type of the VM."
  type        = number
  default     = 1
}

variable "vm_swap_size" {
    description = "The KB of swap size the VM gets."
    type = number
    default = 1024
}

variable "vm_root_password" {
    description = "The password of the root user that is used on this VM."
    type = string
    sensitive = true
}

variable "vm_root_authorized_keys" {
    description = "The ssh key(s) of the root user that is used on this VM."
    type = list(string)
    sensitive = true
}

variable "ansible_playbook_file" {
    description = "The name of the file that is used to configure the VM."
    type = string
}

variable "ansible_vault_password" {
    description = "The password for the vault file that ansible uses."
    type = string
    sensitive = true
}

variable "duck_dns_domain" {
    description = "The domain on duckdns."
    type = string
}

variable "duck_dns_token" {
    description = "The token to access duckdns."
    type = string
    sensitive = true 
}

##############################################

# Local Variables

locals {
  vm_all_tags = concat(var.vm_tags, ["terraform", "almalinux9"])

  linode_vm_types = {
    "1"   = "g6-nanode-1"
    "2"   = "g6-standard-1"
  }
  vm_type = local.linode_vm_types[var.linude_vm_type]

  ansible_base_path = "../../../ansible"
  ansible_playbook_path = join("/", [ local.ansible_base_path, var.ansible_playbook_file])
}
